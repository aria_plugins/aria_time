from geopy.geocoders import GoogleV3
import http.client
import xml.etree.ElementTree as ET
import datetime
import time
import locale

from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        if request.json['lang'] == 'fr':
            locale.setlocale(locale.LC_ALL, 'fr_FR.utf8')
        else:
            locale.setlocale(locale.LC_ALL, 'en_US.utf8')

        outcome = request.json['outcome']
        location="Paris" #Default
        specific_location = False
        if 'entities' in outcome:
            if 'location' in outcome['entities']:
                if len(outcome['entities']['location']) > 0:
                    if 'value' in outcome['entities']['location'][0]:
                        location = outcome['entities']['location'][0]['value']
                        specific_location = True

        geolocator = GoogleV3()
        address, (latitude, longitude) = geolocator.geocode(location)
        d = datetime.datetime.utcnow()
        epoch = datetime.datetime(1970,1,1)
        t = int((d - epoch).total_seconds())
        conn = http.client.HTTPSConnection("maps.googleapis.com")
        conn.request("GET", "/maps/api/timezone/xml?location=" + str(latitude) + "," + str(longitude) + "&timestamp=" + str(t) + "&key=AIzaSyCCGF69MFW6EWR1mo9jyEQXAh75EY1LWMI")
        r1 = conn.getresponse()
        api_response = r1.read()
        root = ET.fromstring(api_response)
        local_timemestamp = t + int(float(root.find("raw_offset").text)) + int(float(root.find("dst_offset").text))
        localtime = datetime.datetime.utcfromtimestamp(local_timemestamp)
        if request.json['lang'] == 'fr':
            answer_time = localtime.strftime('%H:%M et %S secondes')
        else:
            answer_time = localtime.strftime('%I:%M and %S seconds %p')
        if (localtime.strftime("%b %d") != datetime.datetime.now().strftime("%b %d")):
            if request.json['lang'] == 'fr':
                answer_time += ", " + localtime.strftime("%A %d-%m-%Y")
            else:
                answer_time += ", " + localtime.strftime("%A %Y-%m-%d")
        if (specific_location == True):
            if request.json['lang'] == 'fr':
                answer_time += " à " + address
            else:
                answer_time += " in " + address
        if request.json['lang'] == 'fr':
            return "Il est " + answer_time
        else:
            return "It's " + answer_time


if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
