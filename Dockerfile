FROM aria/python:3.4-onbuild
MAINTAINER A.R.I.A <team@a-r-i-a.co>

RUN apt-get update && apt-get install -y locales \
	&& rm -rf /var/lib/apt/lists/*
RUN dpkg-reconfigure locales \
	&& locale-gen C.UTF-8 \
	&& /usr/sbin/update-locale LANG=C.UTF-8
RUN echo 'fr_FR.UTF-8 UTF-8' >> /etc/locale.gen \
	&& echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen \
   && locale-gen

RUN echo python main.py > /aria/scripts/run
